Rails.application.routes.draw do
  devise_for :users

  mount ActionCable.server => '/cable'

  root 'chatrooms#index'

  resources :messages

  resources :users, only:[:new, :create] do
    resources :chatrooms, only: [:index, :show, :create]
  end
end
