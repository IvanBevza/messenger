App.messages = App.cable.subscriptions.create({
  channel: 'MessagesChannel', chat_id: parseInt($("#message_chat_id").val())
},
{
  received: function(data) {
    $("#messages").removeClass('hidden');
    $('#messages').append(this.renderMessage(data));
    $('.dialog-window').animate({scrollTop: 9999});
    $('.input').val('');
  },
  renderMessage: function(data) {
    return `<div class="row mb-4 ml-3 mt-4">
              <div class="col-10">
                <p><b>${data.user_email}</b></p>
                <p>${data.message}</p>
              </div>
                
              <span class="ml-4">${data.created_at}</span>
              <a href="${data.url}" class="delete ml-5"><i class="fas fa-times"></i></a>
            </div>`;
  },
});

