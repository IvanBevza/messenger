class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :messages, dependent: :destroy
  has_many :subscriptions, dependent: :destroy
  has_many :chatrooms, through: :subscriptions

  def existing_chats_users
    existing_chat_users = []
    chatrooms.each do |chat|
      existing_chat_users.concat(chat.subscriptions.where.not(user_id: id).map { |subscription| subscription.user })
    end
    existing_chat_users.uniq
  end
end
