class MessagesController < ApplicationController
  protect_from_forgery with: :null_session, only: :destroy

  def create
    message = Message.new(message_params)
    message.user = current_user
    if message.save
      ActionCable.server.broadcast "messages_#{message_params[:chat_id]}",
                                   message: message.content,
                                   user: message.user.username,
                                   user_email: message.user.email,
                                   created_at: message.created_at.strftime("%H:%M"),
                                   url: url_for(action: :show, id: message)
      head :ok
    else
      redirect_to root_path
    end
  end

  def destroy
    message = Message.find(params[:id])
    message.destroy
    head :no_content
  end

  private

  def message_params
    params.require(:message).permit(:content, :chatroom_id)
  end
end