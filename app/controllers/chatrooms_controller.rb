require 'securerandom'

class ChatroomsController < ApplicationController
  def index
    if current_user.present?
      @chats = current_user.chatrooms
      @existing_chats_users = current_user.existing_chats_users
    end
    @users = User.all
  end

  def show
    @chat = Chatroom.find(params[:id])
    @message = Message.new
    @other_user = User.find(params[:other_user])
  end


  def create
    @other_user = User.find(params[:other_user])
    @chat = find_chat(@other_user) || Chatroom.new(identifier: SecureRandom.hex)
    unless @chat.persisted?
      @chat.save
      @chat.subscriptions.create(user_id: current_user.id)
      @chat.subscriptions.create(user_id: @other_user.id)
    end
    redirect_to user_chatroom_path(current_user, @chat, other_user: @other_user.id)
  end

  private

  def find_chat(second_user)
    chats = current_user.chatrooms
    chats.each do |chat|
      chat.subscriptions.each do |s|
        if s.user_id == second_user.id
          return chat
        end
      end
    end
    nil
  end
end